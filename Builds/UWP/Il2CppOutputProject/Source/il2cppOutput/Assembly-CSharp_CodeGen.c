﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Life::TakesLife(System.Int32)
extern void Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD (void);
// 0x00000002 System.Void Life::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178 (void);
// 0x00000003 System.Void Life::.ctor()
extern void Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926 (void);
// 0x00000004 System.Void ControlGame::Start()
extern void ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225 (void);
// 0x00000005 System.Void ControlGame::Update()
extern void ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50 (void);
// 0x00000006 System.Void ControlGame::LevelPassed()
extern void ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534 (void);
// 0x00000007 System.Void ControlGame::GameOver()
extern void ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13 (void);
// 0x00000008 System.Void ControlGame::Clear()
extern void ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074 (void);
// 0x00000009 System.Void ControlGame::.ctor()
extern void ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D (void);
// 0x0000000A System.Void ControlStart::Start()
extern void ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51 (void);
// 0x0000000B System.Void ControlStart::StartClick()
extern void ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB (void);
// 0x0000000C System.Void ControlStart::Quit()
extern void ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747 (void);
// 0x0000000D System.Void ControlStart::.ctor()
extern void ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA (void);
// 0x0000000E System.Void EnemyControl::Start()
extern void EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F (void);
// 0x0000000F System.Collections.IEnumerator EnemyControl::Process()
extern void EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B (void);
// 0x00000010 System.Void EnemyControl::EnemyCreate()
extern void EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E (void);
// 0x00000011 System.Collections.IEnumerator EnemyControl::CallBoss()
extern void EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101 (void);
// 0x00000012 System.Void EnemyControl::.ctor()
extern void EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF (void);
// 0x00000013 System.Void EnemyControl/<Process>d__4::.ctor(System.Int32)
extern void U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15 (void);
// 0x00000014 System.Void EnemyControl/<Process>d__4::System.IDisposable.Dispose()
extern void U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2 (void);
// 0x00000015 System.Boolean EnemyControl/<Process>d__4::MoveNext()
extern void U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC (void);
// 0x00000016 System.Object EnemyControl/<Process>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B (void);
// 0x00000017 System.Void EnemyControl/<Process>d__4::System.Collections.IEnumerator.Reset()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D (void);
// 0x00000018 System.Object EnemyControl/<Process>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996 (void);
// 0x00000019 System.Void EnemyControl/<CallBoss>d__6::.ctor(System.Int32)
extern void U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363 (void);
// 0x0000001A System.Void EnemyControl/<CallBoss>d__6::System.IDisposable.Dispose()
extern void U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9 (void);
// 0x0000001B System.Boolean EnemyControl/<CallBoss>d__6::MoveNext()
extern void U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909 (void);
// 0x0000001C System.Object EnemyControl/<CallBoss>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9 (void);
// 0x0000001D System.Void EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D (void);
// 0x0000001E System.Object EnemyControl/<CallBoss>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27 (void);
// 0x0000001F System.Void PauseGame::Awake()
extern void PauseGame_Awake_m97F295B24E249FED8B2580B0FE77C9B8451D5442 (void);
// 0x00000020 System.Void PauseGame::Pause()
extern void PauseGame_Pause_mA46E60F35FE723900D21848403D39191F93DED87 (void);
// 0x00000021 System.Void PauseGame::.ctor()
extern void PauseGame__ctor_m30333EB46A97D90A48DA44AC44EF6AD752B62319 (void);
// 0x00000022 System.Void RecordControl::Start()
extern void RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F (void);
// 0x00000023 System.Void RecordControl::UpdateName()
extern void RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC (void);
// 0x00000024 System.Void RecordControl::.ctor()
extern void RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4 (void);
// 0x00000025 System.Void Boss2::Start()
extern void Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8 (void);
// 0x00000026 System.Collections.IEnumerator Boss2::ShowParts()
extern void Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315 (void);
// 0x00000027 System.Void Boss2::Update()
extern void Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14 (void);
// 0x00000028 System.Collections.IEnumerator Boss2::Shot()
extern void Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6 (void);
// 0x00000029 UnityEngine.GameObject Boss2::GetOneActive()
extern void Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3 (void);
// 0x0000002A System.Void Boss2::KillMe(UnityEngine.GameObject)
extern void Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285 (void);
// 0x0000002B System.Void Boss2::.ctor()
extern void Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F (void);
// 0x0000002C System.Void Boss2/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8 (void);
// 0x0000002D System.Void Boss2/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146 (void);
// 0x0000002E System.Boolean Boss2/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704 (void);
// 0x0000002F System.Object Boss2/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41 (void);
// 0x00000030 System.Void Boss2/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D (void);
// 0x00000031 System.Object Boss2/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A (void);
// 0x00000032 System.Void Boss2/<Shot>d__7::.ctor(System.Int32)
extern void U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C (void);
// 0x00000033 System.Void Boss2/<Shot>d__7::System.IDisposable.Dispose()
extern void U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7 (void);
// 0x00000034 System.Boolean Boss2/<Shot>d__7::MoveNext()
extern void U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE (void);
// 0x00000035 System.Object Boss2/<Shot>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6 (void);
// 0x00000036 System.Void Boss2/<Shot>d__7::System.Collections.IEnumerator.Reset()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62 (void);
// 0x00000037 System.Object Boss2/<Shot>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25 (void);
// 0x00000038 System.Void Boss3::Start()
extern void Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C (void);
// 0x00000039 System.Collections.IEnumerator Boss3::ShowParts(System.Boolean)
extern void Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6 (void);
// 0x0000003A System.Collections.IEnumerator Boss3::AttackNow()
extern void Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420 (void);
// 0x0000003B System.Collections.IEnumerator Boss3::Attack(UnityEngine.UI.Image)
extern void Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006 (void);
// 0x0000003C UnityEngine.GameObject Boss3::GetOneActive()
extern void Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F (void);
// 0x0000003D System.Void Boss3::KillMe(UnityEngine.GameObject)
extern void Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029 (void);
// 0x0000003E System.Void Boss3::.ctor()
extern void Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306 (void);
// 0x0000003F System.Void Boss3/<ShowParts>d__5::.ctor(System.Int32)
extern void U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D (void);
// 0x00000040 System.Void Boss3/<ShowParts>d__5::System.IDisposable.Dispose()
extern void U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962 (void);
// 0x00000041 System.Boolean Boss3/<ShowParts>d__5::MoveNext()
extern void U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77 (void);
// 0x00000042 System.Object Boss3/<ShowParts>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F (void);
// 0x00000043 System.Void Boss3/<ShowParts>d__5::System.Collections.IEnumerator.Reset()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524 (void);
// 0x00000044 System.Object Boss3/<ShowParts>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF (void);
// 0x00000045 System.Void Boss3/<AttackNow>d__6::.ctor(System.Int32)
extern void U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9 (void);
// 0x00000046 System.Void Boss3/<AttackNow>d__6::System.IDisposable.Dispose()
extern void U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521 (void);
// 0x00000047 System.Boolean Boss3/<AttackNow>d__6::MoveNext()
extern void U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7 (void);
// 0x00000048 System.Object Boss3/<AttackNow>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA (void);
// 0x00000049 System.Void Boss3/<AttackNow>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A (void);
// 0x0000004A System.Object Boss3/<AttackNow>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0 (void);
// 0x0000004B System.Void Boss3/<Attack>d__7::.ctor(System.Int32)
extern void U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327 (void);
// 0x0000004C System.Void Boss3/<Attack>d__7::System.IDisposable.Dispose()
extern void U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF (void);
// 0x0000004D System.Boolean Boss3/<Attack>d__7::MoveNext()
extern void U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301 (void);
// 0x0000004E System.Object Boss3/<Attack>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52 (void);
// 0x0000004F System.Void Boss3/<Attack>d__7::System.Collections.IEnumerator.Reset()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7 (void);
// 0x00000050 System.Object Boss3/<Attack>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F (void);
// 0x00000051 System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x00000052 System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x00000053 System.Void Enemy::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12 (void);
// 0x00000054 System.Void Enemy::MyDeath()
extern void Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8 (void);
// 0x00000055 System.Collections.IEnumerator Enemy::KillMe()
extern void Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5 (void);
// 0x00000056 System.Void Enemy::Create(System.Int32)
extern void Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759 (void);
// 0x00000057 System.Collections.IEnumerator Enemy::Shoot()
extern void Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767 (void);
// 0x00000058 System.Void Enemy::EndBoss()
extern void Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA (void);
// 0x00000059 System.Void Enemy::PStick()
extern void Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173 (void);
// 0x0000005A System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x0000005B System.Void Enemy/<KillMe>d__11::.ctor(System.Int32)
extern void U3CKillMeU3Ed__11__ctor_m39F55ED326225F6F70DE169AD2263FA6E50709F0 (void);
// 0x0000005C System.Void Enemy/<KillMe>d__11::System.IDisposable.Dispose()
extern void U3CKillMeU3Ed__11_System_IDisposable_Dispose_m2AE1C5C8A05729FFB6C5CC4DFE94459847EE2B64 (void);
// 0x0000005D System.Boolean Enemy/<KillMe>d__11::MoveNext()
extern void U3CKillMeU3Ed__11_MoveNext_mB4CEE7711796F95AA8B7F3236432782907E83424 (void);
// 0x0000005E System.Object Enemy/<KillMe>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CKillMeU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C8E17E4297E6D9C06BF4208D35C9F28F798858F (void);
// 0x0000005F System.Void Enemy/<KillMe>d__11::System.Collections.IEnumerator.Reset()
extern void U3CKillMeU3Ed__11_System_Collections_IEnumerator_Reset_mD9B6FA38233812B8AB5C3217C1DD75370731DEC6 (void);
// 0x00000060 System.Object Enemy/<KillMe>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CKillMeU3Ed__11_System_Collections_IEnumerator_get_Current_m2FA6F373AFD979C18AF6AE19551B73A40AF765C6 (void);
// 0x00000061 System.Void Enemy/<Shoot>d__13::.ctor(System.Int32)
extern void U3CShootU3Ed__13__ctor_m509660EF6044E6F0298FBA22453E53E7EBB151EF (void);
// 0x00000062 System.Void Enemy/<Shoot>d__13::System.IDisposable.Dispose()
extern void U3CShootU3Ed__13_System_IDisposable_Dispose_m3E97D1DF4970C68E17C32FBA15BCEBC114B5E28B (void);
// 0x00000063 System.Boolean Enemy/<Shoot>d__13::MoveNext()
extern void U3CShootU3Ed__13_MoveNext_mD6E962080A9D14A8458E71735A84D99DEC039D56 (void);
// 0x00000064 System.Object Enemy/<Shoot>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE590286AC0CAFC35ABE9B65A43290FCB2AE0BC1D (void);
// 0x00000065 System.Void Enemy/<Shoot>d__13::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__13_System_Collections_IEnumerator_Reset_m72662B20826DEE0604D743F4AEF9BE8B21998231 (void);
// 0x00000066 System.Object Enemy/<Shoot>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__13_System_Collections_IEnumerator_get_Current_mDC23A241C783E9DD2018A924D0994E0E720CF33D (void);
// 0x00000067 System.Void ItemDrop::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7 (void);
// 0x00000068 System.Void ItemDrop::.ctor()
extern void ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2 (void);
// 0x00000069 System.Void CallScene::Call(System.String)
extern void CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE (void);
// 0x0000006A System.Void CallScene::.ctor()
extern void CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C (void);
// 0x0000006B System.Void DestroyTime::Start()
extern void DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497 (void);
// 0x0000006C System.Collections.IEnumerator DestroyTime::CallDestroy()
extern void DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6 (void);
// 0x0000006D System.Void DestroyTime::.ctor()
extern void DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88 (void);
// 0x0000006E System.Void DestroyTime/<CallDestroy>d__2::.ctor(System.Int32)
extern void U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF (void);
// 0x0000006F System.Void DestroyTime/<CallDestroy>d__2::System.IDisposable.Dispose()
extern void U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD (void);
// 0x00000070 System.Boolean DestroyTime/<CallDestroy>d__2::MoveNext()
extern void U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B (void);
// 0x00000071 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2 (void);
// 0x00000072 System.Void DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.Reset()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9 (void);
// 0x00000073 System.Object DestroyTime/<CallDestroy>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0 (void);
// 0x00000074 System.Void Explosion::Start()
extern void Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22 (void);
// 0x00000075 System.Collections.IEnumerator Explosion::Explode()
extern void Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258 (void);
// 0x00000076 System.Void Explosion::.ctor()
extern void Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094 (void);
// 0x00000077 System.Void Explosion/<Explode>d__3::.ctor(System.Int32)
extern void U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F (void);
// 0x00000078 System.Void Explosion/<Explode>d__3::System.IDisposable.Dispose()
extern void U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8 (void);
// 0x00000079 System.Boolean Explosion/<Explode>d__3::MoveNext()
extern void U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC (void);
// 0x0000007A System.Object Explosion/<Explode>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02 (void);
// 0x0000007B System.Void Explosion/<Explode>d__3::System.Collections.IEnumerator.Reset()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7 (void);
// 0x0000007C System.Object Explosion/<Explode>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB (void);
// 0x0000007D System.Void MBuildProcessor::.ctor()
extern void MBuildProcessor__ctor_m06E227BDC15A7F4549638B59EF49C4AFF6844D76 (void);
// 0x0000007E Stick/stck Stick::GetStck()
extern void Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B (void);
// 0x0000007F System.Void ControlShip::Start()
extern void ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8 (void);
// 0x00000080 System.Void ControlShip::LateUpdate()
extern void ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D (void);
// 0x00000081 System.Void ControlShip::AnimateMotor()
extern void ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27 (void);
// 0x00000082 System.Collections.IEnumerator ControlShip::Shoot()
extern void ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA (void);
// 0x00000083 System.Void ControlShip::CallShield()
extern void ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C (void);
// 0x00000084 System.Void ControlShip::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73 (void);
// 0x00000085 System.Void ControlShip::.ctor()
extern void ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93 (void);
// 0x00000086 System.Void ControlShip/<Shoot>d__14::.ctor(System.Int32)
extern void U3CShootU3Ed__14__ctor_m918212AD71D0FFDF17E1552174E7421E325CE1D9 (void);
// 0x00000087 System.Void ControlShip/<Shoot>d__14::System.IDisposable.Dispose()
extern void U3CShootU3Ed__14_System_IDisposable_Dispose_m93F9FD34F6D7228B444A2EA5F811CBEA0677F37F (void);
// 0x00000088 System.Boolean ControlShip/<Shoot>d__14::MoveNext()
extern void U3CShootU3Ed__14_MoveNext_m51766104A83259371BBD8F8DBF6FA6EF3D2A4FB2 (void);
// 0x00000089 System.Object ControlShip/<Shoot>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73A3D6FF335CE4DB189C51BA32DBB48E4DC39194 (void);
// 0x0000008A System.Void ControlShip/<Shoot>d__14::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__14_System_Collections_IEnumerator_Reset_mAC9A39E29B95782473059C03DAACCA94530B5BD8 (void);
// 0x0000008B System.Object ControlShip/<Shoot>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__14_System_Collections_IEnumerator_get_Current_m240B538B60BA691A5D8718C44313F9C7474A2C71 (void);
// 0x0000008C System.Void Flux.ReadMe::.ctor()
extern void ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56 (void);
// 0x0000008D System.Void Gaminho.Level::.ctor()
extern void Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9 (void);
// 0x0000008E System.Void Gaminho.ScenarioLimits::.ctor()
extern void ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B (void);
// 0x0000008F System.Void Gaminho.Shot::.ctor()
extern void Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3 (void);
// 0x00000090 UnityEngine.Quaternion Gaminho.Statics::FaceObject(UnityEngine.Vector2,UnityEngine.Vector2,Gaminho.Statics/FacingDirection)
extern void Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50 (void);
// 0x00000091 System.Void Gaminho.Statics::.cctor()
extern void Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4 (void);
static Il2CppMethodPointer s_methodPointers[145] = 
{
	Life_TakesLife_m432769A0DBB59D454784B3402F19D358B66CBDBD,
	Life_OnTriggerEnter2D_mB5E33226D7DB91144ACC54786FDF08388B017178,
	Life__ctor_m7EA475E113F8E58CDBD7F64DE7C3378510DD7926,
	ControlGame_Start_m41EDFA823653EE35B75C9E396B158F9C47330225,
	ControlGame_Update_m75CA34A327D9AE3F5BE315C068BB2D3946985B50,
	ControlGame_LevelPassed_m05B6235DD8F835987CA547397293A735BB6F6534,
	ControlGame_GameOver_m1E16954A2EB5306CA5D7959354BD4728C0321A13,
	ControlGame_Clear_m0681179893AC0309B4E59F5EA1033BE470C10074,
	ControlGame__ctor_mBB7334A9B707AA5AB4EC6F7C853BE92433BB4D4D,
	ControlStart_Start_m201D1963F24351E276FC706916096DE032376F51,
	ControlStart_StartClick_m4F919D6E668B781C8C5DC0EB2EB21611D882EFAB,
	ControlStart_Quit_mD819D0CAD1831F9E534C96E210F5FFFF9FDB1747,
	ControlStart__ctor_mB1F89FE5B8F09B1F79FC79EA5BFDBDCEBEF790EA,
	EnemyControl_Start_mB2709A00C33F64E801FB336D9224A48BAEDF451F,
	EnemyControl_Process_m3026B4978A8349BD60037F2B25AFA4539B70FB7B,
	EnemyControl_EnemyCreate_m1DBC2B3B2C19C6C6D410EAB1360BB814DD12FD3E,
	EnemyControl_CallBoss_mEE1F75478CABCE55BC6A16FA00D0F843E7A58101,
	EnemyControl__ctor_mE15FA6B97AEFE883D943CC0D237897F435BC34BF,
	U3CProcessU3Ed__4__ctor_mA6DCD4EAD0188CA4C4AD0A5EF0D2A9FC2F8ADC15,
	U3CProcessU3Ed__4_System_IDisposable_Dispose_m382FC3C37C2241AC4E64B8D0EC2C821F916C3DE2,
	U3CProcessU3Ed__4_MoveNext_m3A08B74C2B6D96C327D949D051A8EA8D5B423DDC,
	U3CProcessU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m277414C234EE05878133959D21839B5949AC295B,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_Reset_mE513AF299BB9BF436B27408FF4C0558A73F4676D,
	U3CProcessU3Ed__4_System_Collections_IEnumerator_get_Current_m158EF3608628631AC8F9A0A6CE2568AEB6566996,
	U3CCallBossU3Ed__6__ctor_mDA19F5EC5865BAF920AD4E709D8ADE95E743F363,
	U3CCallBossU3Ed__6_System_IDisposable_Dispose_m68D5010133251BF57F27F51D1BB928B1E4C8DDD9,
	U3CCallBossU3Ed__6_MoveNext_mDF3544E995D32004659624A65FB35CCD76C0B909,
	U3CCallBossU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2297F4117C95DDE24CFD237A355224230FA00BD9,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_Reset_mAB176BC3F3DE4AD36A0EAF25D234733D5CA47B3D,
	U3CCallBossU3Ed__6_System_Collections_IEnumerator_get_Current_mB515EAE6DED7E678D67D764943739ADEF1C84D27,
	PauseGame_Awake_m97F295B24E249FED8B2580B0FE77C9B8451D5442,
	PauseGame_Pause_mA46E60F35FE723900D21848403D39191F93DED87,
	PauseGame__ctor_m30333EB46A97D90A48DA44AC44EF6AD752B62319,
	RecordControl_Start_m8F7A53F239DE398E63E55AA2A9EF390392EB516F,
	RecordControl_UpdateName_m3C7C5ABFF677519820B6F281AF99A768064360DC,
	RecordControl__ctor_m8B9BD294AC3D1EDA3869E09F7EFC58F7B97304C4,
	Boss2_Start_mF5292CA924E8DB443334DB78CCD7201F768E15C8,
	Boss2_ShowParts_mCDC314F0FBDB8D208165FAAE3F8F4D358F7FD315,
	Boss2_Update_m8CE583E0DAC32C0A374F69542AD8C054754C0C14,
	Boss2_Shot_m35B8B00F7AEF10F996EF1D55BEF3575E5BAFF3D6,
	Boss2_GetOneActive_m41CEB8C5D4F936E741F9C9F684F40EAE7D3DADA3,
	Boss2_KillMe_mDE3ED52B5D597F8CEB197D98E1F15E9334C71285,
	Boss2__ctor_mAF0CEE4D38FEAFC1DE100B0F2E0701E06B14991F,
	U3CShowPartsU3Ed__5__ctor_mBDD0E8898D91E138B7042D22F0A5E1456EA501C8,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_m0AC2D2E39174DA2D173BDA8759091EEE1D71F146,
	U3CShowPartsU3Ed__5_MoveNext_m0B40D4DCC0A1EBAC248878B225A64D4925E31704,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F4521C1346516C77ED9C7B550671F928987CE41,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m0468FE7B0426062B26C538D23C7C5FF8CC38570D,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_m20F3D7FA705381FA5AA9C2B5C037BDCB2BC2128A,
	U3CShotU3Ed__7__ctor_mE5DADDE0420441F86985C44BD718DD08A4AD334C,
	U3CShotU3Ed__7_System_IDisposable_Dispose_m8F588491D89FF21288555692FE3A90E505E341F7,
	U3CShotU3Ed__7_MoveNext_m6010D4868EF31AE0774066824F01233F5F37D4AE,
	U3CShotU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m12B604A9AC558372662A0FFD180A325ACDD91EA6,
	U3CShotU3Ed__7_System_Collections_IEnumerator_Reset_mE289E17958922FDDCC8DE7C1FA61940025274E62,
	U3CShotU3Ed__7_System_Collections_IEnumerator_get_Current_m935EF7A6B9356B060AA9DFD3E78AA608C890BC25,
	Boss3_Start_m025FF1DBDC22B6304FBDBAD41E2A028B5BCF4E2C,
	Boss3_ShowParts_m237CC1FA5AE12E3DC8B0F2E864759C88F37E1EC6,
	Boss3_AttackNow_m751CAFCD3CD70F4748E1BE4A61688B418BF83420,
	Boss3_Attack_mBC827254E14F721046A48C74FBDCE480D7EB8006,
	Boss3_GetOneActive_mC454EE4919EB35E3CADF35A2C047B267CBB5754F,
	Boss3_KillMe_m485F8403E9462C6F16E4A1E1346373AE8AE15029,
	Boss3__ctor_m643E6AD2933C0D491F1706E2F4CA3BA2F3820306,
	U3CShowPartsU3Ed__5__ctor_mE02D1048C34075267CA9F8A9AAA9BAA01A251D2D,
	U3CShowPartsU3Ed__5_System_IDisposable_Dispose_mFFAB94361C876791A1720E45B41DDE4920D6E962,
	U3CShowPartsU3Ed__5_MoveNext_mCD5696204CDF2A5ECBC56EF51E055A070AC9FB77,
	U3CShowPartsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m46AC63FF765EC7ABFD50ABD1A0EC06666CCE0B0F,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_Reset_m6B70E7B67CF4CDCB8A4933138894ADD37C944524,
	U3CShowPartsU3Ed__5_System_Collections_IEnumerator_get_Current_mE6AF45BEC418417DB98C019E25CDB0370CB836FF,
	U3CAttackNowU3Ed__6__ctor_m807B06176433FA7EB7E91A0C957DA1F3903A1BB9,
	U3CAttackNowU3Ed__6_System_IDisposable_Dispose_m084196477A3C10B1664186E740CEE59FD3B30521,
	U3CAttackNowU3Ed__6_MoveNext_mEA7C3F8A7C4FA8E6D4387CE9F9A6709AFEB394E7,
	U3CAttackNowU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m340352050CC94EAE9A072030CD314DA56DD329AA,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_Reset_m9E1C7C0E00159B453D194492ECF021B8BB7CD45A,
	U3CAttackNowU3Ed__6_System_Collections_IEnumerator_get_Current_m574DD65751CE17C59EB1B1B4A52EC305243484D0,
	U3CAttackU3Ed__7__ctor_m1E3D32EDA81B13F2EE38F2859D3B2C4F28E9D327,
	U3CAttackU3Ed__7_System_IDisposable_Dispose_m6A015A6CA5CCBB4708004CC3651BB9B1D3725CFF,
	U3CAttackU3Ed__7_MoveNext_mA361D488087872EB4D071C00B7A2E97B943ED301,
	U3CAttackU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF28BAAACFCDE294FEDE76B40F09FF75F8FC16E52,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_Reset_m5A5C9CB773069AF2AEB75EA9A0A398A9459D72B7,
	U3CAttackU3Ed__7_System_Collections_IEnumerator_get_Current_m52CBBA00FF612CAD09F60DF816EAC8E2A237414F,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_OnCollisionEnter2D_m47EAF0D1D60EC5BF3953975EA6BF15189DF82A12,
	Enemy_MyDeath_mA4085ED4BD10E6B3EDEE186807D3C8FB762A4CC8,
	Enemy_KillMe_m24D0CC29C673187902BFD4554072865ADF3F2EB5,
	Enemy_Create_m66ACA885B980722A913D038190E87E6AFFA55759,
	Enemy_Shoot_m9698649D8FCE4A907B5EBAA30105E3DF881FB767,
	Enemy_EndBoss_mA358579DEF2D3DAF68CD80B9AD4C96C1981554DA,
	Enemy_PStick_m5BAC91E00684770270D3B5164328B2EF020D9173,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	U3CKillMeU3Ed__11__ctor_m39F55ED326225F6F70DE169AD2263FA6E50709F0,
	U3CKillMeU3Ed__11_System_IDisposable_Dispose_m2AE1C5C8A05729FFB6C5CC4DFE94459847EE2B64,
	U3CKillMeU3Ed__11_MoveNext_mB4CEE7711796F95AA8B7F3236432782907E83424,
	U3CKillMeU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9C8E17E4297E6D9C06BF4208D35C9F28F798858F,
	U3CKillMeU3Ed__11_System_Collections_IEnumerator_Reset_mD9B6FA38233812B8AB5C3217C1DD75370731DEC6,
	U3CKillMeU3Ed__11_System_Collections_IEnumerator_get_Current_m2FA6F373AFD979C18AF6AE19551B73A40AF765C6,
	U3CShootU3Ed__13__ctor_m509660EF6044E6F0298FBA22453E53E7EBB151EF,
	U3CShootU3Ed__13_System_IDisposable_Dispose_m3E97D1DF4970C68E17C32FBA15BCEBC114B5E28B,
	U3CShootU3Ed__13_MoveNext_mD6E962080A9D14A8458E71735A84D99DEC039D56,
	U3CShootU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE590286AC0CAFC35ABE9B65A43290FCB2AE0BC1D,
	U3CShootU3Ed__13_System_Collections_IEnumerator_Reset_m72662B20826DEE0604D743F4AEF9BE8B21998231,
	U3CShootU3Ed__13_System_Collections_IEnumerator_get_Current_mDC23A241C783E9DD2018A924D0994E0E720CF33D,
	ItemDrop_OnTriggerEnter2D_m1A1B2CBD8DABD7C2C8C666248C9E7B9A56F022A7,
	ItemDrop__ctor_m5D826B2329C00417D6FB12D4C40DE3EB0DB55AF2,
	CallScene_Call_m8CDF9D77505FAEDF29068B890862D09654434CCE,
	CallScene__ctor_m6B52AD5FB87324D006A0BFF36D122D8E5096AE9C,
	DestroyTime_Start_m8C0A193A37746E7A2A2C617E998A31E724373497,
	DestroyTime_CallDestroy_m61D5F2B05283C2474DC66285AC88FDA79B8E1BD6,
	DestroyTime__ctor_mAB5E312F1EAAD1B3B69CC64F2B95B607F8EE4F88,
	U3CCallDestroyU3Ed__2__ctor_m7A7A8DC06CB12FA979DC587DA59CD00B21710FAF,
	U3CCallDestroyU3Ed__2_System_IDisposable_Dispose_m93E09EB5836B606D9F59C3CB667BDD2C8F1215BD,
	U3CCallDestroyU3Ed__2_MoveNext_mCDEF6B443D6ED1BA89399F05AEC81423D64C107B,
	U3CCallDestroyU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA97E34B5CF7FEDA8B2A9EFBCE21A197CE51A2BB2,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_Reset_mEAC272124FCDF01C2C344D034F9CBA6F076E2EF9,
	U3CCallDestroyU3Ed__2_System_Collections_IEnumerator_get_Current_m89A18098EC6562CFAFAD044268F750C304307BA0,
	Explosion_Start_m519BD45EC393F52D86FB64B10889D5CBADCF4C22,
	Explosion_Explode_m0586F5F99D95A44520E522FA9CF6256DCB7FC258,
	Explosion__ctor_m1400515C43124E852380BB8283E15042AF0A5094,
	U3CExplodeU3Ed__3__ctor_mA6402CE95E7EDCBD469BF9A8CE6EE0321AC1C77F,
	U3CExplodeU3Ed__3_System_IDisposable_Dispose_m6323FDA8CD6A90728B774930FBE5BE77024148E8,
	U3CExplodeU3Ed__3_MoveNext_m7FF575367596C970A277CCA7BB7B3D994F32DADC,
	U3CExplodeU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m86DF7C5A40A55D07317D9A2E9F52DC0BCB4C7D02,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_Reset_m487EB8D5E237996B6DEF819E4290C05128D890D7,
	U3CExplodeU3Ed__3_System_Collections_IEnumerator_get_Current_mFD18D2F6818A63E397EFE2758F218DABF2B2F2CB,
	MBuildProcessor__ctor_m06E227BDC15A7F4549638B59EF49C4AFF6844D76,
	Stick_GetStck_m7A49B714FF83A4D429439940FB6990888867BD7B,
	ControlShip_Start_m5B912D419F20ED9BAFF9C7E0981D6ECF66DBA9F8,
	ControlShip_LateUpdate_m8960B65ECF9A86E6205A72BCA648E6A8BF4EDF7D,
	ControlShip_AnimateMotor_m95E9E12E5AB0068BB68740D04C32C91B610BAA27,
	ControlShip_Shoot_mC367083E502F3FB24057683528873F3C1ABE49FA,
	ControlShip_CallShield_m981741EAE83CD99702ECA0E88C92115E68E6CA0C,
	ControlShip_OnCollisionEnter2D_mF0FEC752B221AC776F36E9EB533C3AAD666FBC73,
	ControlShip__ctor_m1DFE695CD1EAB1B2CB9B079E3ED024FF986CFC93,
	U3CShootU3Ed__14__ctor_m918212AD71D0FFDF17E1552174E7421E325CE1D9,
	U3CShootU3Ed__14_System_IDisposable_Dispose_m93F9FD34F6D7228B444A2EA5F811CBEA0677F37F,
	U3CShootU3Ed__14_MoveNext_m51766104A83259371BBD8F8DBF6FA6EF3D2A4FB2,
	U3CShootU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73A3D6FF335CE4DB189C51BA32DBB48E4DC39194,
	U3CShootU3Ed__14_System_Collections_IEnumerator_Reset_mAC9A39E29B95782473059C03DAACCA94530B5BD8,
	U3CShootU3Ed__14_System_Collections_IEnumerator_get_Current_m240B538B60BA691A5D8718C44313F9C7474A2C71,
	ReadMe__ctor_m8FF25B75AB71F389F93E1E2A4DE7D2985ADD6F56,
	Level__ctor_mADE87462A9D13B244DC3F73F22BBA57A3CF8ADD9,
	ScenarioLimits__ctor_mE7ED76B4AD2650003AE4F5A075F9D7F03360730B,
	Shot__ctor_m4BFE80FB650FFA47A20F832227B0A5F9BD84C4B3,
	Statics_FaceObject_mD7C4A0B189730DDA2CC972CE3204E2FE896FAF50,
	Statics__cctor_mB0BED0D7AA2F076698019E572C2CB7E7FEFC3BA4,
};
static const int32_t s_InvokerIndices[145] = 
{
	1229,
	1240,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1413,
	1445,
	1413,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1445,
	1413,
	1445,
	1413,
	1413,
	1240,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1006,
	1413,
	1005,
	1413,
	1240,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1445,
	1240,
	1445,
	1413,
	1229,
	1413,
	1445,
	1445,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1240,
	1445,
	1240,
	1445,
	1445,
	1413,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1413,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	2341,
	1445,
	1445,
	1445,
	1413,
	1445,
	1240,
	1445,
	1229,
	1445,
	1432,
	1413,
	1445,
	1413,
	1445,
	1445,
	1445,
	1445,
	1873,
	2362,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	145,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
