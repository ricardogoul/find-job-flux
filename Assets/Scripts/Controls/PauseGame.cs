using UnityEngine;
using UnityEngine.UI;

public class PauseGame : MonoBehaviour
{
    public AudioSource audio;
    public Text text;

    private bool isPaused = false;

    private void Awake()
    {
        text.text = "Pause";
    }

    public void Pause()
    {
        if (isPaused)
        {
            Time.timeScale = 1;
            audio.UnPause();
            isPaused = false;
            text.text = "Pause";
        }
        else
        {
            Time.timeScale = 0;
            audio.Pause();
            isPaused = true;
            text.text = "Unpause";
        }
    }
}
